# Create your views here.
from .serializers import CandidateListSerializer, CandidateUpdateSerializer, CandidateCreateSerializer
from .models import Candidate
from .demo import create_event
from rest_framework import status
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema

#..............................Candidate views.....................
class CandidateListAPIView(APIView):
    """
    List all candidates, or create a new candidate.
    """
    def get(self, request):
        candidates = Candidate.objects.all()
        serializer = CandidateListSerializer(candidates,many=True)
        return Response(serializer.data)

class CandidateCreateAPIView(APIView):
    """
        Create a new candidate 
    """
    @swagger_auto_schema(request_body=CandidateCreateSerializer)
    def post(self, request, format=None):
        serializer = CandidateCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class CandidateDetailAPIView(APIView):
    """
        Retrieve, update or delete a candidate instance.
    """
    def get_object(self, pk):
        try:
            return Candidate.objects.get(pk=pk)
        except Candidate.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = CandidateListSerializer(snippet)
        return Response(serializer.data)
    
    @swagger_auto_schema(request_body=CandidateUpdateSerializer)
    def patch(self, request, pk):
        candidate_obj = self.get_object(pk)
        serializer = CandidateUpdateSerializer(candidate_obj, data=request.data, partial=True)

        if serializer.is_valid():
            interview_date = serializer.validated_data.get('interview_date')
            post = serializer.validated_data.get('post')
            interview_round = serializer.validated_data.get('interview_round')
            email = serializer.validated_data.get('email')
            
            create_event(interview_date, summary=f"Interview Scheduled for round {interview_round} for {post}", email=email)

            serializer.save()
            resp = {
                "status": "success",
                "message": "candidate item updated",
                "data": serializer.data
            }
            return Response(resp)
        else:
            resp = {
                "status": "failure",
                "message": serializer.errors
            }
            return Response(resp)
        
        
    def delete(self, request, pk, format=None):
        candidate = self.get_object(pk)
        candidate.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)