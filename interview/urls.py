from django.urls import path
from .views import *

urlpatterns = [

    #....................Candidate urls.......................
    path("candidate/list/", CandidateListAPIView.as_view(), name="candidatelistapi"),
    path("candidate/create/", CandidateCreateAPIView.as_view(), name="candidatecreate"),
    path("candidate/details/<int:pk>/", CandidateDetailAPIView.as_view(), name="candidatestatuschangeapi"),
]