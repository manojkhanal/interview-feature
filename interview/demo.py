# OAuth 2.0 Setup
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow 
# Utility Function
import datefinder
from datetime import datetime, timedelta

# Get the scope from the google calendar api scopes
scopes = ['https://www.googleapis.com/auth/calendar']

flow = InstalledAppFlow.from_client_secrets_file("client_secret_key.json", scopes=scopes)

credentials = flow.run_console() 
#After line no 13 is run, it will ask you to sign in and enter the authorization key provided by gmail. A new window will pop up, you should sign in, Allow the permission and insert the key into the terminal.
# print(credentials)

import pickle
pickle.dump(credentials, open("token.pkl", "wb")) #It will dump the credentials in token.pkl file

credentials = pickle.load(open("token.pkl", "rb"))
service = build("calendar", "v3", credentials=credentials)



def create_event(start_time_str, summary, email, duration=1, description=None, location=None):
    start_time_str = start_time_str.strftime("%Y-%m-%dT%H:%M:%S")
    interview = list(datefinder.find_dates(start_time_str))
    email = email
    
    if len(interview):
        start_time = interview[0]
        end_time = start_time + timedelta(hours=duration)
    
    event = {
        'summary': summary,
        'location': location,
        'description': description,
        'start': {
            'dateTime': start_time.strftime("%Y-%m-%dT%H:%M:%S"),
            'timeZone': 'Asia/Kathmandu',
        },
        'end': {
            'dateTime': end_time.strftime("%Y-%m-%dT%H:%M:%S"),
            'timeZone': 'Asia/Kathmandu',
        },
        'attendees': [
        {'email': email},
        

        ],
        'reminders': {
            'useDefault': False,
            'overrides': [
                {'method': 'email', 'minutes': 24 * 60},
                {'method': 'popup', 'minutes': 10},
            ],
        },
    }
    return service.events().insert(calendarId='primary', body=event).execute()

# create_event("16 March 9 PM", "Meeting")
