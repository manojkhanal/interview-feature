from django.db import models
from django.utils.translation import gettext_lazy as _

class Candidate(models.Model):
    full_name = models.CharField(max_length=200)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    post = models.CharField(max_length=100)
    experience = models.CharField(max_length=100, null=True, blank=True)
    expected_salary = models.CharField(max_length=100)
    interview_date = models.DateTimeField(null=True, blank=True)
    interview_round = models.CharField(max_length=100, null=True, blank=True)


    class Meta:
        verbose_name = _("Candidate")
        verbose_name_plural = _("Candidates")
        
    def __str__(self) -> str:
        return self.full_name