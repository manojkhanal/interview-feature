from rest_framework import serializers
from .models import Candidate

# Candidate serializers

class CandidateListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Candidate
        fields = ["id", "full_name", "email", "post", "interview_round", "post", "interview_date"]

class CandidateCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Candidate
        fields = ["id", "full_name", "email", "post", "phone", "experience"]

class CandidateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Candidate
        fields = ["interview_round", "post", "interview_date", "email"]