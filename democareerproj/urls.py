from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="Test Interview API",
        default_version=0.1,
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@interview.local"),
        license=openapi.License(name="Test License"),
        ),
        public = True, 
        permission_classes=[permissions.AllowAny],
        

)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("interview.urls")),
    path('docs/', schema_view.with_ui('swagger',cache_timeout=0), name='schema-swagger-ui')
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
